{application,vector,
             [{applications,[kernel,stdlib,elixir,logger]},
              {description,"Library of common vector functions for use in geometric or graphical calculations.\n"},
              {modules,['Elixir.Vector']},
              {registered,[]},
              {vsn,"1.0.1"}]}.
